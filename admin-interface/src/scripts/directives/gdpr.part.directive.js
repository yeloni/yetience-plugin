angular.module('yetienceApp')
    .directive('gdprPart', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            controller: ['$scope', '$attrs', '$rootScope', '$element', 'UtilsService', '$timeout', function($scope, $attrs, $rootScope, $element, UtilsService, $timeout) {

                console.log("GDPR Part: " + $attrs.gdprPart)

                //assigning id's
                switch ($attrs.gdprPart) {
                    case 'consentCheckbox':
                        $element.prop('id', 'autience-gdpr-consentcheck-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-checkbox-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-consent-checkbox')
                        break;
                    case 'consentText':
                        $element.prop('id', 'autience-gdpr-consenttext-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-consent-text')
                        break;
                    case 'termsCheckbox':
                        $element.prop('id', 'autience-gdpr-termscheck-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-checkbox-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-terms-checkbox')
                        break;
                    case 'termsText':
                        $element.prop('id', 'autience-gdpr-termstext-' + $rootScope.widget.code)
                        $element.addClass('yel-gdpr-terms-text')
                        break;
                    case 'termsContainer':
                        $element.addClass('yel-gdpr-terms-container')
                        break;
                    case 'consentContainer':
                        $element.addClass('yel-gdpr-consent-container')
                        break;
                        

                } //end switch

                $rootScope.$watch('widget.components.gdprPlaceholder.values', function(new_val) {
                    var show_consent = UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'showConsentCheckbox'])
                    var show_terms = UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'showTermsOfService'])
                    var show_privacy = UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'showPrivacyPolicy'])
                    var terms_url = UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'termsOfServiceUrl'])
                    var privacy_url = UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'privacyPolicyUrl'])
                    var terms_html = ''

                    console.log('show_consent ', show_consent)
                    console.log('show_terms ', show_terms)
                    console.log('show_privacy ', show_privacy)
                    console.log('terms_url ', terms_url)
                    console.log('privacy_url ', privacy_url)

                    if ($attrs.gdprPart == 'consentContainer') {
                        console.log('in consentContainer ')
                        if (show_consent) {
                            console.log('Showing')
                            $element.css('display', 'block')

                        } else {
                            console.log('Hiding')
                            $element.css('display', 'none')
                        }
                    }

                    if ($attrs.gdprPart == 'termsContainer') {
                        if (show_terms || show_privacy) {
                            $element.css('display', 'block')
                        } else {
                            $element.css('display', 'none')
                        }
                    }

                    if ($attrs.gdprPart == 'consentText') {
                        console.log('GDPR part is consentText')
                        //set inner html as the consentText
                        $element.html(UtilsService.nestedValue($rootScope.widget, ['components', 'gdprPlaceholder', 'values', 'consentText']))
                    }

                    if ($attrs.gdprPart == 'termsText') {
                        console.log('GDPR part is termsText')
                        //Set inner html based on terms checkbox and urls

                        if (show_terms && show_privacy) {
                            terms_html = 'I agree to the ' + '<a href="' + terms_url + '" target="_blank"> Terms of Service</a>' + ' and ' + '<a href="' + privacy_url + '" target="_blank">Privacy Policy</a>'
                        } else {
                            if (show_terms) {
                                terms_html = 'I agree to the ' + '<a href="' + terms_url + '" target="_blank"> Terms of Service</a>'
                            } else {
                                terms_html = 'I agree to the ' + '<a href="' + privacy_url + '" target="_blank"> Privacy Policy</a>'
                            }
                        }
                        $element.html(terms_html)
                    }

                }, true)

            }]
        }
    }])
