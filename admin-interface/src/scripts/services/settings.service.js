angular.module('yetienceApp')
    .service('SettingsService', ['CommService', '$rootScope', 'configurationFields', '$cookies', '$q', 'UtilsService', '$stateParams', '$location', function (CommService, $rootScope, configurationFields, $cookies, $q, UtilsService, $stateParams, $location) {

        this.hasAllFeatures = false;
        var setup = null
        var encode = function (x) {
            return window.btoa(encodeURIComponent(x))
        }

        var decode = function () {

            return decodeURIComponent(window.atob)
        }
        this.encode = encode

        this.decode = decode

        this.setup = function () {
            return setup
        }

        var action_button_message = "<h3>Almost done!</h3><br/>Your popup will appear when the user clicks on the round floating button. (If you need an exit intent popup, you can select another design)<div class='yel-instructions'><ul><li>Click the button below to Save your Popup</li><li>Open any page on your website</li><li>Wait till the page fully loads</li><li>The round floating button shows up on the bottom right of the page</li><li>The Popup should show up when the user clicks on the button</li><li>After saving, you can configure the Popup behaviour</li></ul></div><br>",
            exit_intent_message = "<h3>Almost done!</h3><br/>Your popup will appear when the visitor attempts to leave the website by moving their mouse towards the browser's close button.<div class='yel-instructions'><ul><li>Click the button below to Save your Popup</li><li>Open any page on your website</li><li>Wait till the page fully loads</li><li>Move your mouse towards the close button</li><li>The Popup should show up</li><li>After saving, you can configure the Popup behaviour</li></ul></div><br>",
            save_message = exit_intent_message

        function getWebsiteAndAttach(website_id, check_saved) {
            //get the latest website instance corresponding to the id

            return CommService.getWebsite(website_id)
                .then(function (website) {
                    // if (website.extensions) {
                    //     if (website.extensions.premiumSubscription) {
                    //         //add other extensions when premiumSubscription is found
                    //         console.log('found premiumSubscription - attaching other extensions')
                    //         var extensions = {}
                    //         extensions.mobileScreens = website.extensions.premiumSubscription
                    //         extensions.specificPages = website.extensions.premiumSubscription
                    //         extensions.premiumSubscription = website.extensions.premiumSubscription
                    //         website.extensions = extensions
                    //             // console.log('website after attaching')
                    //             // console.log(website)
                    //     }
                    // }
                    //console.log('check_saved- ' + check_saved + ' website.saved- ' + website.saved)
                    if (website.saved == check_saved) {
                        var package_upgraded = false,
                            new_extension_purchased = false

                        if (setup.package_id != website.package_id) {
                            package_upgraded = true
                        }

                        if (!angular.equals(setup.extensions, website.extensions)) {
                            new_extension_purchased = true
                        }

                        angular.extend(setup, website)

                        addAdvancedConfigurationOnSetup(setup, website)

                        if (check_saved == false) {
                            saveSetup('', 'Got it. Thanks!', true)
                        } else {
                            if (package_upgraded) {
                                saveSetup("<center>Congratulations! Your are now subscribed to the Premium Version</center>", 'Got it. Thanks', true)
                            }

                            if (new_extension_purchased) {
                                CommService.createNewEvent("d_purchased_extension", '')
                                saveSetup("<center>Congratulations! You have activated an Addon</center>", 'Got it. Thanks', true)
                            }
                        }


                        return $q.resolve(website)
                    } else {
                        return $q.reject()
                    }
                })
        }

        function createWebsiteAndAttach() {
            return CommService.createWebsiteId()
                .then(function (website) {

                    $cookies.put("autience-website-id", website.id)
                    angular.extend(setup, website)

                    //adding the time the plugin is installed first time. 
                    //required for the timebound offer
                    // console.log("SAVING SETUP TIME------");
                    // $rootScope.setupDateTime = new Date().toLocaleString();
                    // console.log($rootScope.setupDateTime);

                    //CommService.createNewEvent("website_created");
                    return $q.resolve(website)
                })
        }

        function checkPoweredByAndAttach(cb) {
            //checks if there is a reffer_id
            //if there is, then mark setup.poweredBy as true, otherwise false
            //once it is done, call the callback
            //Testing
            //1. Initially referrer_id is present
            //2. Initially referrer_id is not present
            //3. Initially referrer_id is present, but later it is not present
            CommService.isReferrerPresent()
                .then(function (referrer_present) {

                    console.log('referrer_present is ' + referrer_present)
                    setup.poweredBy = referrer_present
                    console.log('updated setup is ', setup)
                    cb()
                })
                .catch(function () {
                    cb()
                })

        }

        this.initialize = function (cb) {
            //when the page loads, read the setup and upload it to the server
            var setup_on_platform = yetience.readFromPlatform()
            if (setup_on_platform) {
                setup = setup_on_platform
                $rootScope.SETUP = setup
            }

            var widgets = UtilsService.nestedValue(setup, ['widgets'])
            if (angular.isArray(widgets) && widgets.length > 0) {
                CommService.createNewEvent("d_widget_saved", '')
            }

            var extensions = UtilsService.nestedValue(setup, ['extensions'])
            if (angular.isObject(extensions) && Object.keys(extensions).length > 0) {
                CommService.createNewEvent("d_premium_activated", '')
            }


            if (!setup.id) {
                //console.log('NO WEBSITE ON SETUP')
                //website id is not saved on wordpress
                //check if there is a website_id on intermediate cookie
                if ($cookies.get("autience-website-id")) {
                    //attach website from cookie if, if website.saved is false
                    //console.log('trying with website_id fro cookie- ' + $cookies.get("autience-website-id"))
                    getWebsiteAndAttach($cookies.get("autience-website-id"), false)
                        .then(function () {
                            //console.log("Attached website id from cookie")
                            cb()
                        }, function () {
                            //console.log("Cannot attach website id of cookie. Creating a new one")
                            createWebsiteAndAttach()
                                .then(function () {
                                    checkPoweredByAndAttach(saveSetupAndCallback(cb))
                                })
                        })
                } else {
                    //console.log("there is no website id on cookie")
                    createWebsiteAndAttach()
                        .then(function () {
                            checkPoweredByAndAttach(saveSetupAndCallback(cb))
                        })
                }


            } else {
                //console.log('WEBSITE ID found on setup')

                getWebsiteAndAttach(setup.id, true).then(function () {
                    cb()
                }, function () {
                    //console.log('website is not yet marked as saved.. marking')
                    CommService.markSaved(setup.id).then(function () {
                        //console.log("marked as saved")
                        CommService.createNewEvent("d_fresh_plugin_install", '')

                        cb()
                    })
                })
            }

            function saveSetupAndCallback(cb) {
                return function () {
                    saveSetup('', 'Got it. Thanks!', true)
                    cb()
                }

            }

        }



        function addAdvancedConfigurationOnSetup(setup, website) {
            //check and add extensions
            if (setup.widgets) {

                //Add extension tags on configuration so that corresponding form fields can be enabled/disabled
                for (var i = 0; i < setup.widgets.length; i++) {
                    if (hasFeature('limitByReferrer')) {
                        // console.log('added mobileScreens')
                        setup.widgets[i].configuration.limitByReferrer = true
                    }
                    if (hasFeature('hideAfterCta')) {
                        // console.log('added mobileScreens')
                        setup.widgets[i].configuration.hideAfterCta = true
                    }
                    if (hasFeature('mobileScreens')) {
                        // console.log('added mobileScreens')
                        setup.widgets[i].configuration.mobileScreens = true
                    }
                    if (hasFeature('specificPages')) {
                        // console.log('added specificPages')
                        setup.widgets[i].configuration.specificPages = true
                    }
                    if (hasFeature('showAdminFeature')) {
                        setup.widgets[i].configuration.showAdminFeature = true
                        // console.log(setup.widgets[i].configuration)
                    }
                    if (hasFeature('mobileScreens') || hasFeature('specificPages') || hasFeature('showAdminFeature')) {
                        setup.widgets[i].configuration.advancedConfiguration = true
                    }
                }
            }
        }

        this.saveSetup = saveSetup

        function saveSetup(message, label, disable_undo) {

            console.log('setup before saving')
            console.log(setup)
            yetience.saveToPlatform(setup, message, label, disable_undo)
            //console.log('reading after saving')
            //console.log(yetience.readFromPlatform())
            $rootScope.readyToSave = true
        }

        this.addNewWidget = function (theme_id, widget) {
            //pushing will be done at the end
            //setup.widgets.push(widget)

            widget.theme = theme_id
            console.log("ADDING A NEW WIDGET- " + widget.theme)

            CommService.createNewEvent('d_create_widget', widget);

            var theme = $rootScope.themes[theme_id]
            var categories = []
            categories = theme.categories

            var components = {},
                component_type = null

            //create components object for the widget
            for (var tag in theme.components) {
                component_type = theme.components[tag].component

                components[tag] = {
                    tag: tag,
                    type: component_type,
                    title: theme.components[tag].title
                }

                //components[tag].fields = $rootScope.components[component_type].customizations
                if ($rootScope.components[component_type]) {
                    components[tag].fields = $rootScope.components[component_type].fields
                } else {
                    console.log('ERROR: component ' + component_type + ' is not defined for tag ' + tag)
                }

                components[tag].values = {}
            }

            widget.components = components

            /*
            //configuration is used from the default configuration of the category

            widget.configuration = $rootScope.categories[theme.categories[0]].default
            console.log($rootScope.categories[theme.categories[0]].default)
            */
            //Starting from default configuration defined in configuration.constant
            widget.configuration = configurationFields.default

            //Check if there is already a domain for this setup. If domain exists,mark it on configuration.what

            widget.configuration.what.existing_domain = setup.domain
            widget.configuration.what.existing_account = setup.account_id

            //set premium user as true if it is a premium user
            //widget.configuration.premiumUser = true
            if (setup.features && setup.features.indexOf("advancedConfiguration") >= 0) {

                widget.configuration.advancedConfiguration = true

            }

            CommService.getThemeTemplate(theme_id)
                .then(function (template) {
                    //console.log(template)
                    widget.raw = encode(template)
                    widget.rendered = encode(template)
                })
        }

        this.hasFeature = hasFeature

        function hasFeature(feature) {
            var now = Math.round(new Date().getTime() / 1000);
            if (setup) {
                if (setup.package_id != 'default') {
                    return true
                }
                //All features if premiumSubscription is present
                if (setup.extensions && setup.extensions.premiumSubscription && setup.extensions.premiumSubscription > now) {
                    this.hasAllFeatures = true;
                    return true
                }
                if (setup.extensions && setup.extensions[feature] && setup.extensions[feature] > now) {
                    return true
                }
            }

            return false
        }

        this.extensionCount = function () {
            if (setup) {
                return Object.keys(setup.extensions).length
            }

            return 0
        }

        this.isPremium = function () {
            if (setup.package_id && setup.package_id != 'default') {
                return true
            }
        }

        this.showAffiliateLink = function (show) {

            setup.showAffiliateLink = show;
            saveSetup("Please click on the 'Save Changes' button below", "Save Changes", false);
        }


        this.createWidget = function (theme) {
            return {
                code: UtilsService.getRandomCode(),
                initialization: {},
                theme: theme
            }
        }

        this.getWidget = function (index) {
            return setup.widgets[index]
        }

        this.pushWidget = pushWidget

        function pushWidget(widget) {
            setup.widgets.push(widget)
        }

        this.setWidget = setWidget

        function setWidget(widget, index) {
            setup.widgets[index] = widget
        }


        this.SaveCurrentPopup = function () {
            console.log('Calling Save Popup')

            //prefix and postfix to add for the google analytics tracking script

            var tracking_template_prefix = "<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', '"
            var tracking_template_postfix = "', 'auto'); ga('send', 'pageview'); </script>"

            //For google analytics - add rest of the script if only analytics code is added
            if ($rootScope.widget.components.commonanalytics.values.measureAnalyticsBoolean == true) {
                var tracking_code = $rootScope.widget.components.commonanalytics.values.analyticsTrackingCode
                if (tracking_code) {
                    if (tracking_code.indexOf('<script>') < 0) {
                        $rootScope.widget.components.commonanalytics.values.analyticsTrackingCode = tracking_template_prefix + tracking_code + tracking_template_postfix
                    }
                }
            }

            if (document.getElementById('yel-basic-support')) {
                document.getElementById('yel-basic-support').style.display = 'none'
            }

            if (document.getElementById('yel-premium-support')) {
                document.getElementById('yel-premium-support').style.display = 'none'
            }


            //for adding premium provider as provider
            if ($rootScope.premiumProviderSelected) {
                $rootScope.widget.components.emailSubscription.values.provider = $rootScope.widget.components.emailSubscription.values.premiumProvider
                console.log('premiumProvider added')
                //end premium provider part
            }
            /*
            if (!$rootScope.widget.configuration.what.name) {
                $rootScope.widget.configuration.what.name = "My First Popup"
            }
            */
            //if create mode, then add to SettingsService.setup
            //otherwise update based on index
            var mode = $stateParams.mode
            var index = $location.search().index
            console.log('mode - ' + mode + ' index- ' + index)
            if (mode == 'create') {
                console.log('pushing widget')
                //CommService.createNewEvent('new_widget_saved','')
                //checking if there are no widgets created earlier and only then creating first_widget_time
                if (!setup.first_widget_time) {
                    console.log('adding first_widget_time')
                    setup.first_widget_time = Math.round(new Date().getTime() / 1000);
                }
                $rootScope.widget.widget_time = Math.round(new Date().getTime() / 1000);
                pushWidget($rootScope.widget)
                //Show the save popup message for action buttons or popups
                if ($rootScope.widget.themeType == 'ActionButtons') {
                    save_message = action_button_message
                }
                saveSetup(save_message + ' If something does not work as expected, please contact our Support Team using the Live Chat below', 'Click here to Save')
            } else {
                //CommService.createNewEvent('widget_updated','')
                setWidget($rootScope.widget, index)
                saveSetup('<center>This Widget has been Updated</center>', 'Save')
            }


        }
    }])