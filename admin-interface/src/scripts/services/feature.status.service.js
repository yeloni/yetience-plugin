angular.module('yetienceApp')
    .service('FeatureStatusService', ['$rootScope', function ($rootScope, SettingsService) {


        this.mobileScreens = function (widget, feature) {
            feature.priority = 1;
            return {
                display: true,
                current: "Shows only on desktops",
                ideal: "Show on mobiles & desktops",
                icon: "glyphicon glyphicon-phone",
                upgrade: 'Show on Mobiles and Tablets',
                description: 'Show Popups to Visitors from Mobile devices. Mobile Exit Intent is triggered when the visitor taps the Back button on the Browser ot the Touchpad',
                image: 'https://yeloni.com/wp-content/uploads/2019/02/Mobile-and-tablets-addon.jpg',
                url: 'https://www.yeloni.com/product/show-mobiles-tablets/',
                wcId: '1627',
                enabled: true
            };
        }

        this.specificPages = function (widget, feature) {
            feature.priority = 2;

            return {
                display: true,
                current: "Shows up on all pages",
                ideal: "Show on selected pages",
                icon: "glyphicon glyphicon-list-alt",
                upgrade: 'Show only on Selected Pages',
                description: "Enhance User Experience by showing Popup only on relevant pages. You can create multiple Popups and customize them according to the Content or product",
                image: 'https://yeloni.com/wp-content/uploads/2019/02/page-selection-addon.jpg',
                url: 'https://www.yeloni.com/product/show-selected-pages/',
                wcId: '1628',
                enabled: true
            };
        }

        this.hideAfterCta = function (widget, feature) {
            feature.priority = 6;
            return {
                display: true, //($rootScope.themes[widget.theme].categories.indexOf('subscribe') >= 0),
                current: "Show even after visitor subscribes",
                ideal: "Disable after subscription",
                icon: "glyphicon glyphicon-flash",
                upgrade: 'Show until Expected Action',
                description: "This addon helps you to keep the widget active on multiple pages throughout the visitor's session until they perform the expected action",
                image: 'https://www.yeloni.com/wp-content/uploads/2019/01/stick_around_addon.jpg',
                url: 'https://www.yeloni.com/product/hide-popup-action/',
                wcId: '1633',
                enabled: false
            };
        }

        this.emailSignature = function (widget, feature) {
            feature.priority = 5;
            return {
                display: true,
                current: "",
                ideal: "ideal",
                icon: "glyphicon glyphicon-tag",
                upgrade: "Disable all Yeloni Branding",
                description: "Remove the footnote under the Popup",
                image: 'https://yeloni.com/wp-content/uploads/2019/02/unbranding-addon.jpg',
                url: 'https://www.yeloni.com/product/remove-yeloni-email-signature-autoresponder-emails/',
                wcId: '1629',
                enabled: true
            };
        }

        this.customHtml = function (widget, feature) {
            feature.priority = 4;
            return {
                display: false,
                current: "Make your own widget",
                ideal: "custom html >",
                icon: "glyphicon glyphicon-list-alt",
                upgrade: 'Add Custom HTML',
                description: "If you have specific requirements or an in-house designer, this extension allows you to create popups with your own HTML and CSS",
                image: 'https://yeloni.com/wp-content/uploads/2019/02/custom-html-addon.jpg',
                url: 'https://www.yeloni.com/product/custom-html-popup/',
                wcId: '1630',
                enabled: true
            };
        }

        this.premiumEmail = function (widget, feature) {
            feature.priority = 3;
            return {
                display: false,
                current: "Connect with email provider",
                ideal: " >",
                icon: "glyphicon glyphicon-envelope",
                upgrade: 'Email Marketing Integrations',
                description: 'Forward subscriber contacts from Yeloni with Aweber, Active Campaign and Sendy automatically. Let us know if you use any other Email Marketing service',
                image: 'https://yeloni.com/wp-content/uploads/2019/02/email-marketing-addon.jpg',
                url: 'https://www.yeloni.com/product/connect-email-provider/',
                wcId: '1631',
                enabled: true
            };
        }

        this.showAdminFeature = function (widget, feature) {
            feature.priority = 7;
            return {
                display: true,
                current: "Shows to logged in users",
                ideal: "Hide from logged in users",
                icon: "glyphicon glyphicon-user",
                upgrade: 'Guest Only',
                description: "This allows you to disable widgets for users who are already logged in. Works only if you are using wordpress to manage your users",
                image: 'https://yeloni.com/wp-content/uploads/2019/02/guest-only-addon.jpg',
                url: 'https://www.yeloni.com/product/hide-popup-logged-visitors/',
                wcId: '1632',
                enabled: false
            }
        }

        this.limitByReferrer = function (widget, feature) {
            feature.priority = 8;
            return {
                display: true,
                current: "Show/hide widget to visitors from specific websites",
                ideal: "Hide from logged in users",
                icon: "glyphicon glyphicon-globe",
                upgrade: 'Segment by Source',
                description: 'This allows you to segment your audience based on the referring site. For example, you could show one widget to search traffic (from google) and another widget for social traffic (from facebook)',
                image: 'https://yeloni.com/wp-content/uploads/2019/02/segment-by-source-addon.jpg',
                url: 'https://www.yeloni.com/product/show-hide-popup-based-referrer/',
                wcId: '1634',
                enabled: false
            };
        }

        this.premiumSubscription = function (widget, feature) {
            feature.priority = 9;
            return {
                display: true,
                current: "",
                ideal: "All the above for 10$/month",
                icon: "glyphicon glyphicon-usd",
                upgrade: 'All Features Listed Below',
                description: 'In this plan you get all the above 8 addons at the same price of 2 addons. Its not a surprise that most of our users prefer this plan :)',
                image: 'https://yeloni.com/wp-content/uploads/2019/02/All-Features.jpg',
                url: 'https://www.yeloni.com/product/all-in-one-subscription/',
                wcId: '1626',
                enabled: true
            };
        }

    }])