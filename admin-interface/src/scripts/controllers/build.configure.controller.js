angular.module('yetienceApp')
    .controller('buildConfigureController', ['$scope', 'configurationFields', 'SettingsService', 'CommService', 'UtilsService', '$rootScope', '$state', '$location', function($scope, configurationFields, SettingsService, CommService, UtilsService, $rootScope, $state, $location) {
        $scope.R = $rootScope
        $scope.C = CommService

        if (!UtilsService.checkNested($rootScope, ['widget', 'rendered'])) {
            console.log('rootScope does not have widget.rendered')
            $state.go('build.select')
        }

        $scope.configFields = angular.copy(configurationFields)
        $scope.emailFields = angular.copy($rootScope.components['email-subscriber'].fields)


        $scope.description = function() {
            return $rootScope.basePath + '/src/partials/configuration.description.html'
        }

        $scope.viewAddonsClick = function() {
            console.log("view clicked");
            CommService.createNewEvent('d_configuration_to_premium', '')
            $rootScope.configureModal.close();
            $location.path('/extensions')
        }

        console.log($scope.configFields)
            //attach categories as options where key is where_categories
        $scope.configFields.where.forEach(function(where_field) {

            if (where_field.key == 'where_categories' || where_field.key == 'where_categories_hide') {

                //console.log('Assigned categories')
                where_field.templateOptions.options = yetience.categories
            }

            if (where_field.key == 'where_titles' || where_field.key == 'where_titles_hide' || where_field.key == 'where_url_matches' || where_field.key == 'hide_where_url_matches' || where_field.key == 'hide_where_url_contains') {
                where_field.templateOptions.options = yetience.pageList
            }
        })

        var page_list_autocomplete = []
        for (var i = yetience.pageList.length - 1; i >= 0; i--) {
            var temp = {}
            temp.text = yetience.pageList[i].post_title
            temp.name = yetience.pageList[i].post_name
            page_list_autocomplete.push(temp)
        }

        $scope.loadItems = function() {
            return page_list_autocomplete
        }
        $scope.sections = {
            whom: 'How often should the popup show up?',
            when: 'When should the popup show up?',
            where: 'On which pages should the popup show up? (Premium)',
            close: 'How should the popup close?',
            showAdmin: 'Who sees the popup?'
        }

        $scope.now = {
            editing: 'whom'
        }

        $scope.saving = false


        $scope.clickedSection = function(section) {
            //CommService.createNewEvent('d_configuration_section_clicked', { section: section })
        }

        var modes = ['when', 'where', 'whom', 'how', 'close']
            //if user does not have advancedConfiguration, but selects it, then go to premium screen
        $scope.$watchGroup(['widget.configuration.when.mode', 'widget.configuration.where.mode', 'widget.configuration.whom.mode', 'widget.configuration.how.mode', 'widget.configuration.close.mode'], function(newModes) {

            for (var i in newModes) {
                if (newModes[i] == 'advanced' && !SettingsService.hasFeature('advancedConfiguration')) {

                    //CommService.createNewEvent("d_advanced_setting_clicked", { section: modes[i] });
                    UtilsService.premiumMessage('Advanced configuration options are available on a premium plan. Please Upgrade.')
                        //reset configuration to default
                    for (var section in $scope.sections) {
                        $scope.widget.configuration[section].mode = 'default'
                    }
                    break
                }
            }

        })

        $scope.clearValues = function(value) {
            var values_list = ['hide_match_tags', 'hide_contain_tags', 'show_match_tags', 'show_contain_tags']
            console.log('from RS for ' + value + ' is ')
            for (var i = values_list.length - 1; i >= 0; i--) {
                if (values_list[i] == value) {
                   
                    console.log($rootScope.widget.configuration.where[value])
                }else{
                     $rootScope.widget.configuration.where[values_list[i]] = []
                     console.log(values_list[i] +'clearing value from ',$rootScope.widget.configuration.where[values_list[i]] )
                }
            }
        }

    }])
