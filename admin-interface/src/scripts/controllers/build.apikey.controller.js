angular.module('yetienceApp')
    .controller('buildAPIKeyController', ['$scope', 'SettingsService', '$state', '$rootScope', '$stateParams', '$location', 'WidgetUpdate', 'localStorageService', 'UtilsService', function ($scope, SettingsService, $state, $rootScope, $stateParams, $location, WidgetUpdate, localStorageService, UtilsService) {
        //console.log('INSIDE BUIlD APIKEY CONTROLLER')

        $scope.SS = SettingsService


        $scope.submitCode = function (apikey) {
            if (apikey.trim() == SettingsService.setup().id) {
                //WidgetUpdate.operations.saveTemplate()
                console.log('widget is ', $scope.widget)
                WidgetUpdate.savePopup($scope.widget)
            } else {
                alert('Invalid API Key')
            }
        }

        $scope.domain = function () {
            return location.host
        }
    }])