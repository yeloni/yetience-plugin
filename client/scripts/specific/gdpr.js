window.defineAutienceGDPR = function (yetience_callback) {
    //console.log('injecting email.js')
    if (Autience) {

        Autience.lifecycle.postRender.push(function (widget) {
            // console.log('Inside GDPR client side code')
            toggleSubmitButton(widget)

            Autience.utils.classListen('yel-gdpr-checkbox-' + widget.code, 'click', function (el) {
                toggleSubmitButton(widget)
            })
        })



        if (yetience_callback) {
            yetience_callback()
        }

    }

    function toggleSubmitButton(widget) {
        var submit_id = 'autience-emailform-submit-' + widget.code
        var gdpr_checkboxes = document.getElementsByClassName('yel-gdpr-checkbox-' + widget.code)
        var num_checked = 0

        //  console.log('checkboxes ', gdpr_checkboxes)

        var show_consent = Autience.utils.nestedValue(widget, ['components', 'gdprPlaceholder', 'values', 'showConsentCheckbox'])
        var show_terms = Autience.utils.nestedValue(widget, ['components', 'gdprPlaceholder', 'values', 'showTermsOfService']) || Autience.utils.nestedValue(widget, ['components', 'gdprPlaceholder', 'values', 'showPrivacyPolicy'])
        //  console.log("consent = ", show_consent, " show terms = ", show_terms);
        var num_to_be_checked = show_consent + show_terms


        for (var i = 0; i < gdpr_checkboxes.length; i++) {
            num_checked = num_checked + gdpr_checkboxes[i].checked
        }
        // console.log('num_to_be_checked ', num_to_be_checked, 'num_checked ', num_checked)

        var submit_button = document.getElementById(submit_id)
        if (submit_button) {
            if (num_checked == num_to_be_checked) {
                //submit_button.style.visibility = 'visible'
                submit_button.classList.remove("yel-grayed-button");
            } else {
                //submit_button.style.visibility = 'hidden'
                submit_button.classList.add("yel-grayed-button");
            }

        }
    }



};