window.defineAutienceWhere = function (yetience_callback) {
    // //for display by contain url
    Autience.lifecycle.displayValidation.push(function (widget) {
        //honor show on homepage condition
        if (location.pathname == '/' && (Autience.utils.nestedValue(widget, ['configuration', 'where', 'home']) == true)) {
            return true
        }
        if (Autience.utils.nestedValue(widget, ['configuration', 'where', 'show_contain_tags'])) {
            var contain_tags = widget.configuration.where.show_contain_tags
            if (contain_tags.length == 0) {
                return true
            }
            console.log('location.pathname is ' + location.pathname)
            for (var i = contain_tags.length - 1; i >= 0; i--) {

                if (location.pathname.indexOf(contain_tags[i].text) > -1) {
                    console.log(location.pathname + ' has ' + contain_tags[i].text)
                    return true
                }
            }
            // console.log('contain tags')
            // console.log(contain_tags)
        } else {

            return true
        }


    })

    //for show by match url
    Autience.lifecycle.displayValidation.push(function (widget) {
        // console.log('location.pathname is ' + location.pathname)
        // console.log(widget.configuration.where)
        if (Autience.utils.nestedValue(widget, ['configuration', 'where', 'show_match_tags'])) {
            //honor show on homepage condition
            if (location.pathname == '/' && (Autience.utils.nestedValue(widget, ['configuration', 'where', 'home']) == true)) {
                return true
            }
            var match_tags = widget.configuration.where.show_match_tags
            if (match_tags.length == 0) {
                return true
            }
            // console.log('location.pathname is ' + location.pathname)
            for (var i = match_tags.length - 1; i >= 0; i--) {
                // console.log('------------------')
                console.log(location.pathname + ' is /' + match_tags[i].name)
                if (location.pathname.indexOf(match_tags[i].name) >= 0) {
                    return true
                }
            }
            // console.log('match tags')
            // console.log(match_tags)
        } else {
            return true
        }

    })

    Autience.lifecycle.displayValidation.push(function (widget) {
        //return true = show, false = show
        var showAdmin = widget.configuration.showAdmin

        if (showAdmin != null) { //by default it's not defined
            if (!showAdmin) { //if it is set to false - don't show to logged in users
                if (is_logged_in) {
                    // console.log('dont show popup to admin - logged in as admin')
                    return false
                } else {
                    // console.log('dont show popup to admin - not logged in')
                    return true
                }
            }
        }
        return true

    })
    // //for display by contain url
    Autience.lifecycle.displayValidation.push(function (widget) {
        //return true = show, false = hide
        //honor show on homepage condition
        if (location.pathname == '/' && (Autience.utils.nestedValue(widget, ['configuration', 'where', 'home']) == true)) {
            return true
        }
        if (Autience.utils.nestedValue(widget, ['configuration', 'where', 'hide_contain_tags'])) {
            //honor show on homepage condition
            if (location.pathname == '/' && (Autience.utils.nestedValue(widget, ['configuration', 'where', 'home']) == true)) {
                return true
            }

            var contain_tags = widget.configuration.where.hide_contain_tags
            if (contain_tags.length == 0) {
                return true
            }
            console.log('location.pathname is ' + location.pathname)
            for (var i = contain_tags.length - 1; i >= 0; i--) {
                if (location.pathname.indexOf(contain_tags[i].text) > -1) {
                    console.log(location.pathname + ' has ' + contain_tags[i].text)
                    return false
                }
            }
            // console.log('contain tags')
            // console.log(contain_tags)
        }
        return true

    })

    //for hide by match url
    Autience.lifecycle.displayValidation.push(function (widget) {
        //return true = show, false = hide
        // console.log('location.pathname is ' + location.pathname)
        // console.log(widget.configuration.where)
        if (Autience.utils.nestedValue(widget, ['configuration', 'where', 'hide_match_tags'])) {
            //honor show on homepage condition
            if (location.pathname == '/' && (Autience.utils.nestedValue(widget, ['configuration', 'where', 'home']) == true)) {
                return true
            }
            var match_tags = widget.configuration.where.hide_match_tags
            if (match_tags.length == 0) {
                return true
            }
            console.log('location.pathname is ' + location.pathname)
            for (var i = match_tags.length - 1; i >= 0; i--) {
                console.log('------------------')
                console.log(location.pathname + ' is /' + match_tags[i].name)
                if (location.pathname.indexOf(match_tags[i].name) >= 0) {
                    return false
                }
            }
            // console.log('match tags')
            // console.log(match_tags)
        }
        return true

    })

    Autience.lifecycle.displayValidation.push(function (widget) {
        //return true = show, false = hide
        var showAdmin = widget.configuration.showAdmin

        if (showAdmin != null) { //by default it's not defined
            if (!showAdmin) { //if it is set to false - don't show to logged in users
                if (is_logged_in) {
                    // console.log('dont show popup to admin - logged in as admin')
                    return false
                } else {
                    // console.log('dont show popup to admin - not logged in')
                    return true
                }
            }
        }
        return true

    })

    Autience.lifecycle.displayValidation.push(function (widget) {
        var isMobile = Autience.utils.isMobile()

        if (isMobile && !Autience.utils.hasFeature('mobileScreens')) {
            console.log('Popup is not shown on mobile in lite version')
            return false
        }
        if (isMobile && Autience.utils.hasFeature('mobileScreens')) {
            if (Autience.utils.nestedValue(widget, ['configuration', 'when', 'smallEnabled'])) {
                console.log('enabled on small screen')
                return true
            } else {
                console.log('disabled on small screens')
                return false
            }
        }
        // console.log('default')
        return true
    })


    //show on specific pages
    Autience.lifecycle.displayValidation.push(function (widget) {
        var where = widget.configuration.where
        var cat = null
        var where_categories = widget.configuration.where_categories
        var where_titles = widget.configuration.where_titles

        //sometimes the value given by wordpress is not correct
        autience_is_home = autience_is_home || (window.location.pathname == '/') || (window.location.pathname == '')
        if (autience_is_home) {
            //console.log('where.home is ', where.home)
            return where.home
        }

        if (window.autience_page_name == 'checkout') {
            return where.checkout
        }

        switch (where.other) {
            case 'all':
                return true
            case 'none':
                return false
            case 'specific':
                switch (where.specific.selector) {
                    case 'pageType':
                        switch (window.autience_post_type) {
                            case 'post':
                                return where.pageTypes.posts
                            case 'product':
                                return where.pageTypes.products
                            case 'page':
                                return where.pageTypes.pages
                        }
                        break;
                    case 'category':

                        for (var i = 0; i < window.autience_categories.length; i++) {
                            cat = autience_categories[i].cat_ID
                            if (where_categories.indexOf(cat) >= 0 || where_categories.indexOf(cat.toString()) >= 0) {

                                return true
                            }
                        }

                        console.log('returning false')
                        return false
                        break;
                    case 'title':

                        var index = where_titles.indexOf(window.autience_post_id)

                        console.log('title at ' + index)
                        return (index >= 0)
                        break;
                    case 'url':
                        return true
                        break;
                    case 'custom':
                        //check if the current page type matches the custom page type
                        console.log('Current post type is ', window.autience_post_type)
                        console.log('post type to check is ', where.pageTypes.custom)

                        return (window.autience_post_type.toLowerCase() == where.pageTypes.custom.toLowerCase())
                }

        }

        return true
    })

    //for hide on specific pages
    Autience.lifecycle.displayValidation.push(function (widget) {
        var where = widget.configuration.where
        var cat = null
        var where_categories_hide = widget.configuration.where_categories_hide
        var where_titles_hide = widget.configuration.where_titles_hide

        // console.log('widget is')
        // console.log(widget)

        if (where.showOrHide == 'hide' && where.hideOn && where.hideOn.hideselector) {
            switch (where.hideOn.hideselector) {
                case 'pageType':
                    switch (window.autience_post_type) {
                        case 'post':
                            return !(where.pageTypes.posts)
                        case 'product':
                            return !(where.pageTypes.products)
                        case 'page':
                            return !(where.pageTypes.pages)
                    }
                    break;
                case 'category':

                    for (var i = 0; i < window.autience_categories.length; i++) {
                        cat = autience_categories[i].cat_ID
                        if (where_categories_hide.indexOf(cat) >= 0 || where_categories_hide.indexOf(cat.toString()) >= 0) {

                            return false
                        }
                    }
                    return true
                    break;
                case 'title':

                    var index = where_titles_hide.indexOf(window.autience_post_id)

                    // console.log('title at ' + index)
                    //return true - show
                    return (index < 0)

                    break;
                case 'url':
                    //see the logic in first two displayValidation
                    return true
                case 'custom':
                    //check if the current page type matches the custom page type
                    console.log('Current post type is ', window.autience_post_type)
                    console.log('post type to check is ', where.pageTypes.custom)

                    return !(window.autience_post_type.toLowerCase() == where.pageTypes.custom.toLowerCase())

            }



            return false
        } else {
            return true
        }
    })




    if (yetience_callback) {
        yetience_callback()
    }
};