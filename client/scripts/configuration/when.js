window.defineAutienceWhen = function (yetience_callback) {

    //log for popup displayed
    Autience.lifecycle.display.push(function (widget) {
        //checking if the popup code is present and sending popup_displayed event
        if (document.getElementById(widget.code)) {
            var code = document.getElementById(widget.code).innerHTML
            if (code) {
                Autience.utils.sendEvent('d_popup_displayed');
                if (Autience.utils.nestedValue(widget, ['widget_time'])) {
                    Autience.utils.sendEvent('popup_liked', {
                        widget_time: widget.widget_time
                    });
                }
            } else {
                Autience.utils.sendEvent('popup_empty');
            }
        } else {
            console.log('Widget ' + widget.code + ' is not loaded')
        }

    })

    //Zopim related functionality
    Autience.lifecycle.display.push(function (widget) {

        if (Autience.utils.nestedValue(widget, ['components', 'zopimChat']) && typeof $zopim !== 'undefined') {
            $zopim(function () {

                var yel_body_height = window.innerHeight
                var yel_body_width = window.innerWidth
                var yel_zopim_height = 400
                var yel_zopim_width = 310
                var yel_popup_offset = 76

                $zopim.livechat.window.show();
                var yel_loc = document.getElementById("yel-chat-wrapper").getBoundingClientRect();
                //console.log(yel_body_width)
                //console.log(yel_loc.left)
                //console.log(yel_loc.top)

                $zopim.livechat.window.setOffsetHorizontal(yel_body_width - yel_zopim_width - yel_loc.left - 5);
                $zopim.livechat.window.setOffsetVertical((yel_body_height - yel_zopim_height) - yel_popup_offset);

                if (yel_body_width < 767) {
                    $zopim.livechat.window.setOffsetVertical((yel_body_height - yel_zopim_height) - yel_loc.top);
                    $zopim.livechat.window.setOffsetHorizontal((yel_body_width - yel_zopim_width) / 2);
                }

            });
        }
    })

    //attach listener to display when the event occurs
    Autience.lifecycle.onPageLoad.push(function (widget) {
        // console.log('Attaching display on trigger')
        //first check if the  widget is enabled
        if (Autience.utils.nestedValue(widget, ['configuration', 'what', 'enable'])) {
            var when = widget.configuration.when

            var is_mobile = Autience.utils.isMobile()
            // console.log('is mobile- ' + is_mobile)
            var different_for_mobiles = when.smallDifferent
            var device = 'large',
                delay = 0,
                links_to_match = null,
                prevent_redirect = false


            if (is_mobile && different_for_mobiles) {
                device = 'small'
            }

            var trigger = when[device]
            var autience_event = trigger
            switch (trigger) {
                //handle these trigger cases differently
                case 'scroll':
                    autience_event = 'scroll_' + when.scroll[device]
                    break
                case 'delay':
                    autience_event = 'load'
                    delay = when.delay[device]
                    break
                case 'link':
                    var link_type = when.link[device]
                    autience_event = 'link_' + link_type

                    if (link_type == 'custom') {
                        links_to_match = when.customLink[device]
                    }
                    //Autience['disable_link_' + link_type] = true
                    prevent_redirect = true
                    break
            }

            displayPopupOnEvent(autience_event, delay, links_to_match, prevent_redirect)
            widget.trigger = {
                trigger: trigger,
                autience_event: autience_event,
                delay: delay
            }

        } else {
            Autience.utils.sendEvent('client_widget_disabled')
            console.log('widget is disabled')
        }

        function displayPopupOnEvent(autience_event, delay, links_to_match, prevent_redirect) {
            //Listen to the defined event and run the display lifecycle
            //console.log('Attached Event Listener for Popup for event ', autience_event)
            Autience.utils.listenAutienceEvent(autience_event, function (evt) {
                // console.log('Autience event triggered ', autience_event)
                // console.log('Autience event is' + autience_event + ' Check if the clicked link matches ', evt)

                if (matchingLink(links_to_match)) {
                    if (prevent_redirect && Autience.clickEvent) {
                        //alert('preventDefault for clickEvent')
                        Autience.clickEvent.preventDefault()
                    }

                    setTimeout(function () {
                        Autience.utils.cycle(Autience.lifecycle.display, widget)
                        // console.log('Popup is triggered')
                        //Autience.utils.sendEvent('popup_triggered')
                    }, delay * 1000)
                }
                //alert('hold on')


            })
        }

        function matchingLink(links) {
            //check links with Autience.current_link
            if (!links) {
                //there is no link, so no need to check
                return true
            }

            var matched = false
            links.split(',').map(function (link) {
                link = link.trim()

                matched = matched || directMatch(Autience.current_link, link) || matchWithoutProtocol(Autience.current_link, link) || matchWithoutHost(Autience.current_link, link)
            })

            return matched

            function directMatch(link1, link2) {
                link1 = stripTrailingSlash(link1)
                link2 = stripTrailingSlash(link2)
                return link1 == link2
            }

            function matchWithoutProtocol(link1, link2) {
                link1 = stripTrailingSlash(link1)
                link2 = stripTrailingSlash(link2)
                link1 = link1.replace('http://', '')
                link1 = link1.replace('https://', '')
                link2 = link2.replace('http://', '')
                link2 = link2.replace('https://', '')
                return link1 == link2
            }

            function matchWithoutHost(link1, link2) {
                link1 = stripTrailingSlash(link1)
                link2 = stripTrailingSlash(link2)
                link1 = link1.replace('http://', '')
                link1 = link1.replace('https://', '')
                link2 = link2.replace('http://', '')
                link2 = link2.replace('https://', '')
                link1 = link1.replace('www.', '')
                link2 = link2.replace('www.', '')
                link1 = link1.replace(window.location.host, '')
                link2 = link2.replace(window.location.host, '')

                return link1 == link2
            }

            function stripTrailingSlash(str) {
                if (str.substr(-1) === '/') {
                    return str.substr(0, str.length - 1);
                }
                return str;
            }
        }

    })


    //adding the analytics tracking code to the body
    Autience.lifecycle.display.push(function (widget) {


        // if (widget.components.commonanalytics.values.measureAnalyticsBoolean && widget.components.commonanalytics.values.analyticsTrackingCode) {
        if ((Autience.utils.nestedValue(widget, ['components', 'commonanalytics', 'values', 'measureAnalyticsBoolean'])) && (Autience.utils.nestedValue(widget, ['components', 'commonanalytics', 'values', 'analyticsTrackingCode']))) {

            var yel_measure_analytics = widget.components.commonanalytics.values.measureAnalyticsBoolean;
            var yel_tracking_code = widget.components.commonanalytics.values.analyticsTrackingCode;


            //adding the analytics script
            if (document.body != null) {
                var tracking_code_div = document.createElement("script");
                tracking_code_div.type = "text/javascript";

                var yel_popup_name = 'Yeloni'

                // if (widget.components.commonanalytics.values.analyticsPopupName)
                if ((Autience.utils.nestedValue(widget, ['components', 'commonanalytics', 'values', 'analyticsPopupName']))) {
                    var yel_temp_popup_name = widget.components.commonanalytics.values.analyticsPopupName;
                    var yel_popup_name = 'Yeloni-' + yel_temp_popup_name.split(' ').join('-');
                }


                //removing <script> tags
                yel_tracking_code = yel_tracking_code.replace("<script>", " ");
                yel_tracking_code = yel_tracking_code.replace("</script>", " ");

                //removing new lines
                yel_tracking_code = yel_tracking_code.replace(/\n/g, " ");
                //yel_tracking_code = yel_tracking_code.replace("pageview", "Page-Load");

                //adding the code to the script
                tracking_code_div.innerHTML = yel_tracking_code;


                document.body.appendChild(tracking_code_div);

                //send the popup display event
                //sending the page load event
                if (typeof ga === "function") {
                    ga('send', 'event', yel_popup_name, 'Popup-Display');
                }
            }
        }

    })

    Autience.lifecycle.render.push(function (widget) {
        var extensions = Autience.utils.nestedValue(Autience, ['setup', 'extensions'])
        //dont show for premium users
        if (extensions && Object.keys(extensions).length > 0) {
            return
        }


        // if (document.getElementsByClassName('logged-in').length > 0) {
        //     return
        // }

        var widget_element = document.getElementById(widget.code)
        var poweredBy = document.createElement('div')
        var url = 'https://yeloni.com'
        url = url + '?utm_source=' + window.location.hostname + '&utm_campaign=poweredby&from_site=' + window.location.hostname

        var message = 'Get an <b>Email Optin</b> for your Website'
        if (widget.goalType != 'subscribe') {
            message = 'Powered by <b>Yeloni</b>'
        }

        poweredBy.innerHTML = "<a href='" + url + "' target='_blank'>" + message + "</a>"

        var style = poweredBy.style,
            astyle = poweredBy.getElementsByTagName('a')[0].style
        Object.assign(style, {
            color: 'white',
            fontSize: '15px',
            fontFamily: 'arial',
            textAlign: 'center'
        })
        Object.assign(astyle, {
            color: 'white'
        })
        widget_element.appendChild(poweredBy)
    })
    /*
    //showing the affiliate link if applicable
    Autience.lifecycle.render.push(function(widget) {

        //console.log("show link-- "+Autience.setup.showAffiliateLink)


        // if (Autience.setup.showAffiliateLink)         {
        if ((Autience.utils.nestedValue(Autience, ['setup', 'poweredBy']))) {
            var yelPopups = document.getElementsByClassName('yel-popup-template'),
                noOfPopups = yelPopups.length,
                shareMessage = 'Sharing Powered by <span style="text-decoration: underline;">Yeloni</span>',
                emailMessage = 'Subscription Powered by <span style="text-decoration: underline;">Yeloni</span>',
                message = 'Powered by <span style="text-decoration: underline;">Yeloni</span>'

            while (noOfPopups--) {
                //add an element at the end of the yel-popup-template div
                var currentNode = yelPopups[noOfPopups]
                var newNode = document.createElement("div")
                newNode.className = "yel-powered"

                if (widget.isSocial && widget.isSocial == true) {
                    message = shareMessage
                } else if (widget.isEmail && widget.isEmail == true) {
                    message = emailMessage
                } else {
                    message = message
                }

                newNode.innerHTML = message

                //so that powered by yeloni shows up only once per widget
                if (!widget.insertedBranding) {
                    currentNode.parentNode.insertBefore(newNode, currentNode.nextSibling)
                    widget.insertedBranding = true
                }
                //Set the branding on bottom right
                document.getElementsByClassName('yel-powered')[noOfPopups].style = 'float:right;margin-right:2%;position: absolute;right: 0px;bottom: 20px;';

            }
            //console.log("autience setup")
            //console.dir(Autience.setup.affiliate_code)

            Autience.utils.classListen('yel-powered', 'click', function() {
                var linkhere = "?utm_source=" + Autience.setup.initial_domain + "&utm_medium=poweredBy"
                var utmLink = "http://www.yeloni.com" + linkhere;
                window.open(utmLink, '_blank');
            })
        }

    })
    */

    if (yetience_callback) {
        yetience_callback()
    }
};